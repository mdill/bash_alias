#!/bin/bash

## Custom alias signals

# Random
alias starwars='telnet towel.blinkenlights.nl'
alias cls='printf "\033c"'
alias sizes='ls -lrSa'
alias diskspace='du -S | sort -n -r |more'
alias grep='egrep -n --color=auto'
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'
alias tree='tree -Csuh'
alias dmesg='dmesg -T'
alias findorphans='find / -nouser -ls'
alias reloadvm='sudo modprobe vboxdrv'
alias logout='xfce4-session-logout --logout'
alias sshlogs='journalctl -u sshd | less'

alias functions='set | grep "()"'
alias bfunctions='cat /etc/bash.bashrc | grep -v "| grep" | grep -v "fixpgp" | grep "()"'

alias fucking='sudo'
alias fuck='sudo $(history -p !!)'

# Pacman alias shortcuts
alias autoremove='sudo pacman -Rns $(pacman -Qtdq)'
alias cacheclean='sudo pacman -Sc'

# APT alias shortcuts
alias apt-update='sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade && sudo apt-get autoremove'

# BREW alias shortcuts
alias brewup='brew update; brew upgrade; brew prune; brew cleanup; brew doctor'

# Networking
alias pingg='ping -c 3 google.com'

# SAV
alias savscan='sudo /opt/sophos-av/bin/savscan'
alias savupdate='sudo /opt/sophos-av/bin/savupdate'
alias savdstatus='/opt/sophos-av/bin/savdstatus'

# Directory
alias ls='ls --color=auto'
alias la='ls -A'
alias ll='ls -lF'
alias lla='ls -alF'
alias l='ls -CF'

# CD
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

