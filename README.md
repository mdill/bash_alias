# BASH alias compilation

## Purpose

This file is simply a compilation of BASH aliases which I use most often.  It is
subject to change, as time goes on.  Some of these are useful, some of them are
silly, and some of them are just garbage.  Regardless, they should be nice for
*somebody* out there.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_alias.git

## Placement

This script is best placed within your `bashrc` (for Linux),
`/etc/bash.bashrc` (for Linux, globally), or `.bash_profile` script, or sourced
within one of those files.

It can also be made into its own file in `~/.bash_aliases` and will
automatically be sourced above.

For a global alternative, you can add it to `/etc/profile.d/00-aliases.sh` to
automatically be sourced.  If this option is chosen, your `bashrc` must also
contain:

    if [ -f /etc/profile.d/00-aliases.sh ]; then
	    . /etc/profile.d/00-aliases.sh
    fi

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_alias/src/4c58c734657012f09c17d32337dae39993708c3a/LICENSE.txt?at=master) file for
details.

